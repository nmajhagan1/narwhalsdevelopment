ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.PrintName = "Halo Reach Fraggrenade"
ENT.Author = ""
ENT.Information = ""
ENT.Spawnable = false
ENT.AdminSpawnable = false

ENT.Model = "models/halo/halo_reach/weapons/frag_grenade.mdl"
ENT.FuseTime = 3
ENT.ArmTime = 2.5
ENT.ImpactFuse = true

ENT.BlastRadius = 450

ENT.BlastDMG = 1600

AddCSLuaFile()

function ENT:Initialize()
    if SERVER then
	util.SpriteTrail( self, 0, Color(255,185,5,155), false, 5, 0, 0.3, 1, "effects/halo2/contrail_human" )
        self:SetModel( self.Model )
        self:SetMoveType( MOVETYPE_VPHYSICS )
        self:SetSolid( SOLID_VPHYSICS )
        self:PhysicsInit( SOLID_VPHYSICS )
        self:SetCollisionGroup( COLLISION_GROUP_PROJECTILE )
        self:DrawShadow( true )

        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
		phys:SetMass(1)
            phys:SetBuoyancyRatio(0)
        end

        self.kt = CurTime() + self.FuseTime
        self.at = CurTime() + self.ArmTime
		self.bt = CurTime() + 1
    end
end

function ENT:PhysicsCollide(data, physobj)
    if SERVER then
        if data.Speed > 75 then
           sound.Play( "halo/halo_reach/weapons/frag_bnc_metal" .. math.random(1,4) .. ".ogg",  self:GetPos(), 100, 100 )
	 local effectdata = EffectData()
        effectdata:SetOrigin( self:GetPos() )
        util.Effect( "StunstickImpact", effectdata)
        elseif data.Speed > 25 then
		sound.Play( "halo/halo_reach/weapons/frag_bnc_stone" .. math.random(1,3) .. ".ogg",  self:GetPos(), 100, 100 )
        end

        if self.at <= CurTime() and self.ImpactFuse then
            self:Detonate()
        end
		
        end
	
    end
 

function ENT:Think()
    if SERVER and CurTime() >= self.kt then
        self:Detonate()
    end
end

function ENT:Detonate()
    if SERVER then
        if not self:IsValid() then return end
        local effectdata = EffectData()
        effectdata:SetOrigin(self:GetPos() + Vector(0,0,25))

        if self:WaterLevel() >= 1 then
            util.Effect( "WaterSurfaceExplosion", effectdata )
		sound.Play( "halo/halo_3/frag_expl_water" .. math.random(1,5) .. ".ogg",  self:GetPos(), 100, 100 )
        else
            ParticleEffect( "astw2_halo_3_frag_explosion", self:GetPos(), self:GetAngles() )
        end
		
		local inflictor = self
		
		if IsValid(self.Owner) then inflictor = self.Owner end
	util.Decal( "astw2_halo_reach_impact_soft_terrain_explosion", self:GetPos(), self:GetPos() - Vector(0, 0, 32), self )
        util.BlastDamage(self, inflictor, self:GetPos(), self.BlastRadius, self.BlastDMG)
		sound.Play( "halo/halo_reach/weapons/frag_reach" .. math.random(2,11) .. ".ogg",  self:GetPos(), 100, 100 )
	util.ScreenShake(self:GetPos(),10000,100,0.8,1024)
        self:Remove()

    end
end

function ENT:Draw()
    if CLIENT then
        self:DrawModel()
		local light = DynamicLight(self:EntIndex())
        if (light) then
            light.Pos = self:GetPos()
            light.r = 255
            light.g = 115
            light.b = 0
            light.Brightness = 6
            light.Decay = 2
            light.Size = 16
            light.DieTime = CurTime() + 0.1
        end
    end
end