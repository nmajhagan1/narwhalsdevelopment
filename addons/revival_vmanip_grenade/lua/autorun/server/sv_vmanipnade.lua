util.AddNetworkString( "VManip_Nade" )
util.AddNetworkString( "VManip_NadeThrow" )
util.AddNetworkString( "VManip_NadeBlowHands" )

vmanip_nadeinertia = CreateConVar( "sv_vmanip_grenadeinertia", 0, { FCVAR_ARCHIVE }, "0: Grenades go where you aim - 1: Grenades are influenced by inertia" )


net.Receive("VManip_Nade", function(len, ply)

if ply:GetAmmoCount( 10 ) < 0 then return end
ply:SetAmmo(ply:GetAmmoCount( 10 )-1, 10)
ply.vmanipfrag = ents.Create("revival_frag")
ply.vmanipfrag:SetParent(ply)
ply.vmanipfrag:SetPos(ply:EyePos() + ( ply:GetAimVector()*-16 )+(ply:GetRight()*-6) )
ply.vmanipfrag:SetAngles( ply:EyeAngles() )
ply.vmanipfrag:SetSaveValue("m_hThrower",ply)
ply.vmanipfrag:SetNoDraw(true)
ply.vmanipfrag:Spawn()

ply.vmanipfrag:SetNotSolid( true )
ply.vmanipfrag:SetHealth(99999) --without this the grenade can get blow up by another one
ply.vmanipfrag:Fire("SetTimer",2.85,0)
ply.vmanipfrag.timer = CurTime()

end)


net.Receive("VManip_NadeThrow", function(len,ply)

if !IsValid(ply.vmanipfrag) then
	return
end

vmanipthrownade(ply,ply.vmanipfrag)

end)
 


function vmanipthrownade(ply,frag)
frag:SetParent(nil)
frag:SetNoDraw(false)
frag:SetNotSolid( false )
ply.vmanipfrag:SetPos(ply:EyePos() + ( ply:GetAimVector() * -16 )+(ply:GetRight()*-5) )

frag.timer = frag.timer-CurTime()
if vmanip_nadeinertia:GetBool() then
	frag.velocity = ply:GetAimVector()+ply:GetVelocity()/450
else
	frag.velocity = ply:GetAimVector()*1.25
end
frag.phys = frag:GetPhysicsObject()
frag.velocity = frag.velocity * 1000
frag.phys:ApplyForceCenter( frag.velocity )
frag.phys:AddAngleVelocity(Vector(500,600,0))
frag:SetOwner(ply)

ply.vmanipfrag:Fire("SetTimer",frag.timer+3,0)
end


hook.Add("PlayerPostThink","VManip_NadeThink",function(ply)

if IsValid(ply.vmanipfrag) and !ply:Alive() and ply.vmanipfrag:GetParent() == ply then
	ply.vmanipfrag:SetParent(nil) ply.vmanipfrag:SetNoDraw(false)
end

if IsValid(ply.vmanipfrag) then
	if CurTime()-ply.vmanipfrag.timer>=3 and ply.vmanipfrag:GetParent()==ply then net.Start("VManip_NadeBlowHands") net.Send(ply) end
end
	

end)


