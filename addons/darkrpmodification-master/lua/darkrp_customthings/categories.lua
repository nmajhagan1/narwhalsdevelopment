--[[-----------------------------------------------------------------------
Categories
---------------------------------------------------------------------------
The categories of the default F4 menu.

Please read this page for more information:
http://wiki.darkrp.com/index.php/DarkRP:Categories

In case that page can't be reached, here's an example with explanation:

DarkRP.createCategory{
    name = "Citizens", -- The name of the category.
    categorises = "jobs", -- What it categorises. MUST be one of "jobs", "entities", "shipments", "weapons", "vehicles", "ammo".
    startExpanded = true, -- Whether the category is expanded when you open the F4 menu.
    color = Color(0, 107, 0, 255), -- The color of the category header.
    canSee = function(ply) return true end, -- OPTIONAL: whether the player can see this category AND EVERYTHING IN IT.
    sortOrder = 100, -- OPTIONAL: With this you can decide where your category is. Low numbers to put it on top, high numbers to put it on the bottom. It's 100 by default.
}


Add new categories under the next line!
---------------------------------------------------------------------------]]
DarkRP.createCategory{
    name = "UNSC",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 51, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 10, 
}
DarkRP.createCategory{
    name = "HQ Command",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(210, 180, 140, 255),
    canSee = function(ply) return true end, 
    sortOrder = 20, 
}
DarkRP.createCategory{
    name = "Lima 1-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(33, 115, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 30, 
}
DarkRP.createCategory{
    name = "Lima 2-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(81, 65, 44, 255),
    canSee = function(ply) return true end, 
    sortOrder = 40, 
}
DarkRP.createCategory{
    name = "Lima 2-2 Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(218, 242, 67, 255),
    canSee = function(ply) return true end, 
    sortOrder = 50, 
}
DarkRP.createCategory{
    name = "Lima 2-3 Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(210, 180, 140, 255),
    canSee = function(ply) return true end, 
    sortOrder = 60, 
}
DarkRP.createCategory{
    name = "Lima 3-1 Platoon",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(204, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 70, 
}   
DarkRP.createCategory{
    name = "Chimera",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 130, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 90, 
}
DarkRP.createCategory{
    name = "Valkyrie",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 0, 0, 255),
    canSee = function(ply) return true end,   
    sortOrder = 100, 
}
DarkRP.createCategory{
    name = "Cerberus",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(62, 44, 90, 255),
    canSee = function(ply) return true end, 
    sortOrder = 110, 
}
DarkRP.createCategory{
    name = "Demon Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(55, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 120, 
}
DarkRP.createCategory{
    name = "Inferno Squad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end, 
    sortOrder = 130, 
}
DarkRP.createCategory{
    name = "Hellfire",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(122, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 140, 
}
DarkRP.createCategory{
    name = "Military Police",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(68, 0, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 150, 
}
DarkRP.createCategory{
    name = "Swords of Sanghelios",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(127, 0, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 160, 
}
DarkRP.createCategory{
    name = "Silent Shadow",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 165, 
}
DarkRP.createCategory{
    name = "Hearts of Jiralhanae",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 17, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 170, 
}
DarkRP.createCategory{
    name = "Scorned",
    categorises = "jobs",
    startExpanded = false,
    color = Color(255, 255, 153, 255),
    canSee = function(ply) return true end,
    sortOrder = 180,
}
DarkRP.createCategory{
    name = "UNSC Central Fleet",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 190, 
}
DarkRP.createCategory{
    name = "Naval Operations",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 200, 
}
DarkRP.createCategory{
    name = "Task Groups",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 205, 
}
DarkRP.createCategory{
    name = "Swordfish",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(102, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 210, 
}
DarkRP.createCategory{
    name = "Reaper",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 220, 
}
DarkRP.createCategory{
    name = "MAJCOM",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(10, 10, 10, 250),
    canSee = function(ply) return true end, 
    sortOrder = 230, 
}
DarkRP.createCategory{
    name = "NAVSPECWAR", -- NAVSPECWAR
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 240, 
}
DarkRP.createCategory{
    name = "Men Of Steel",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(200, 50, 0, 250),
    canSee = function(ply) return true end, 
    sortOrder = 250, 
}
DarkRP.createCategory{
    name = "ODST Command",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 260, 
}
DarkRP.createCategory{
    name = "Bullfrogs",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(54, 47, 47, 255),
    canSee = function(ply) return true end, 
    sortOrder = 270, 
}
DarkRP.createCategory{
    name = "Alpha Six",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(165, 165, 165, 250),
    canSee = function(ply) return true end, 
    sortOrder = 280, 
}
DarkRP.createCategory{
    name = "Tombstone",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(109,109, 109, 255),
    canSee = function(ply) return true end, 
    sortOrder = 290, 
}
DarkRP.createCategory{
    name = "Freelancer",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end, 
    sortOrder = 300, 
}
DarkRP.createCategory{
    name = "Hellhound Squad Lead",
    categorises = "jobs",
    startExpanded = true,
    color = Color(32, 32, 32, 255),
    canSee = function(ply) return true end,
    sortOrder = 310,
}
DarkRP.createCategory{
    name = "ONI",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(160, 160, 160, 255),
    canSee = function(ply) return true end, 
    sortOrder = 320, 
}
DarkRP.createCategory{
    name = "ONI Security Force",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 330, 
}
DarkRP.createCategory{
    name = "AI",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 51, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 340, 
}
DarkRP.createCategory{
    name = "S.W.O.R.D.",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(60,179,113,255),
    canSee = function(ply) return true end, 
    sortOrder = 350, 
}
DarkRP.createCategory{
    name = "Spartan Command Staff",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(105, 105, 105, 255),
    canSee = function(ply) return true end, 
    sortOrder = 360,
}
DarkRP.createCategory{
    name = "Xerxes",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(105, 105, 105, 255),
    canSee = function(ply) return true end, 
    sortOrder = 365, 
}
DarkRP.createCategory{
    name = "Eden",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(199, 175, 31, 255),
    canSee = function(ply) return true end, 
    sortOrder = 370, 
}
DarkRP.createCategory{
    name = "Nomad",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(210, 180, 140, 255),
    canSee = function(ply) return true end, 
    sortOrder = 380, 
}
DarkRP.createCategory{
    name = "Terra",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(33, 115, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 385, 
}
DarkRP.createCategory{
    name = "Eclipse",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(51, 51, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 390, 
}
DarkRP.createCategory{
    name = "Exodus",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 152, 255, 255),
    canSee = function(ply) return true end, 
    sortOrder = 400, 
}
DarkRP.createCategory{
    name = "Nexus",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(6, 16, 105, 255),
    canSee = function(ply) return true end, 
    sortOrder = 410, 
}
DarkRP.createCategory{
    name = "Ares",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 140, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 420, 
}
DarkRP.createCategory{
    name = "Titan",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 140, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 430, 
}
DarkRP.createCategory{
    name = "Warden",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(255, 140, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 431, 
}
DarkRP.createCategory{
    name = "Forerunner",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(100, 100, 100, 255),
    canSee = function(ply) return true end, 
    sortOrder = 440, 
}
DarkRP.createCategory{
    name = "Insurrection",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(53, 53, 53, 255),
    canSee = function(ply) return true end, 
    sortOrder = 450, 
}
DarkRP.createCategory{
    name = "UNSC Headquarters Element",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(121, 23, 196, 255),
    canSee = function(ply) return true end, 
    sortOrder = 460, 
}
DarkRP.createCategory{
    name = "Viking Binary",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(0, 0, 0, 255),
    canSee = function(ply) return true end, 
    sortOrder = 470, 
}
DarkRP.createCategory{
    name = "Admin",
    categorises = "jobs", 
    startExpanded = true,
    color = Color(230,12,40, 255),
    canSee = function(ply) return true end, 
    sortOrder = 480, 
}