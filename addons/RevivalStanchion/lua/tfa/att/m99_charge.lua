if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Charged Shot"
ATTACHMENT.ShortName = "Perk" --Abbreviation, 5 chars or less please
ATTACHMENT.Description = { TFA.Attachments.Colors["+"], "Consumes the magazine and loads a more powerful round.", TFA.Attachments.Colors["-"], "More recoil" }
ATTACHMENT.Icon = "" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.WeaponTable = {} --put replacements for your SWEP talbe in here e.g. ["Primary"] = {}

ATTACHMENT.WeaponTable = {
	["Primary"] = {
		["Damage"] = function(wep,stat) return stat * 5 end,
		["ClipSize"] = 1,
		["BlowbackCurrent"] = 1.3,
		["DamageType"] 	= DMG_BLAST
	}
}

function ATTACHMENT:Attach(wep)
	wep.Weapon:SetClip1(0)
	wep:Reload( true )
	local att = wep:GetMuzzleAttachment()
	--[[
	fx = EffectData()
	fx:SetOrigin(wep:GetOwner():GetShootPos())
	fx:SetNormal(wep:GetOwner():EyeAngles():Forward())
	fx:SetEntity(wep)
	fx:SetAttachment(att)
	TFA.Effects.Create(wep:GetStat("MuzzleFlashEffect" or "tbolt_core_ozone"), fx)
	
	--ParticleEffectAttach("tbolt_core_ozone", PATTACH_POINT_FOLLOW, ply, 1)
	]]--
	timer.Simple(3.2, function() wep:ChargedShot(1) end)
	timer.Simple(3.2, function() wep.Skin = 1 end)
end

function ATTACHMENT:Detach(wep)
	wep.Skin = 0
	wep.Weapon:SetClip1(0)
	wep:Reload( true )
	wep.BlowbackCurrent = 0
	wep:ChargedShot(0)
end


if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end