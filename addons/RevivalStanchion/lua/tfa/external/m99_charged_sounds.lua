local path = "weapons/izanagis/"

TFA.AddWeaponSound("TFA_IZANAGI_FIRE.1", { path .. "IzanagisFire1.wav", path .. "IzanagisFire2.wav", path .. "IzanagisFire3.wav"}, ")" )
TFA.AddWeaponSound("TFA_IZANAGI_IRONIN.1", path .. "IzanagisIn.wav")
TFA.AddWeaponSound("TFA_IZANAGI_IRONOUT.1", path .. "IzanagisOut.wav")
TFA.AddWeaponSound("TFA_IZANAGI_RELOAD.1", path .. "IzanagisReload.wav")
TFA.AddWeaponSound("TFA_IZANAGI_HONEDRELOAD.1", path .. "IzanagisHonedReload.wav")
TFA.AddWeaponSound("TFA_IZANAGI_2XFIRE.1", path .. "IzanagisHonedx2Fire.wav")
TFA.AddWeaponSound("TFA_IZANAGI_4XFIRE.1", path .. "IzanagisHonedx4Fire.wav")

local icol = Color( 255, 100, 0, 255 ) 
if CLIENT then
	killicon.Add(  "destiny_izanagis",	"vgui/killicons/destiny_izanagis", icol  )
end

if CLIENT then
	surface.CreateFont( "TFA_D2_NORMAL", {
		font = "D2 Normal", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
		extended = false,
		size = 28,
		weight = 0,
		antialias = true,
		italic = false
	} )
end