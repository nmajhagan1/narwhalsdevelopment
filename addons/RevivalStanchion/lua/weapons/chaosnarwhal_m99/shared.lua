SWEP.Author 						= "ChaosNarwhal | Arthur | Meliodas"
SWEP.Contact 						= "ChaosNarwhal#9953"
SWEP.Purpose 						= "SPARTAN ONLY SNIPER RIFLE."
SWEP.Base 							= "tfa_gun_base"
SWEP.Category 						= "Project Reclamation"
SWEP.Spawnable 						= false
SWEP.AdminSpawnable 				= true
SWEP.data 							= {}
SWEP.Slot							= 3				-- Slot in the weapon selection menu.  Subtract 1, as this starts at 0.
SWEP.SlotPos						= 56			-- Position in the slot

SWEP.PrintName 						= "M99A2S3"

SWEP.ViewModel						= "models/chaosnarwhal/spartanarmory/m99/SASR-M99-STANCHION.mdl"
SWEP.WorldModel						= "models/chaosnarwhal/spartanarmory/m99/SASR-M99-STANCHION_W.mdl"
SWEP.ViewModelFOV 					= 70
SWEP.HoldType 						= "ar2"
SWEP.ReloadHoldTypeOverride 		= "ar2"

SWEP.Scoped 						= false

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

SWEP.DisableChambering 				= true
SWEP.Primary.ClipSize 				= 4
SWEP.Primary.DefaultClip 			= 10000
SWEP.Primary.Knockback = 0

SWEP.Primary.Sound 					= Sound ("TFA_IZANAGI_FIRE.1")
SWEP.Primary.Ammo 					= "sniperpenetratedround"
SWEP.Primary.Automatic 				= false
SWEP.Primary.RPM 					= 150
SWEP.Primary.Damage 				= 2500
SWEP.Primary.HullSize = 10
SWEP.DamageType 					= DMG_BULLET
SWEP.Primary.DamageBackup			= SWEP.Primary.Damage
SWEP.Primary.NumShots 				= 1
SWEP.Primary.Spread					= .008					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .0004	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
	
SWEP.Primary.KickUp					= 0					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0					-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax 	= 4.8 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement 		= 0.8 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery 		= 4.5 --How much the spread recovers, per second.

SWEP.BoltAction 					= false --Un-sight after shooting?
SWEP.BoltTimerOffset 				= 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos 					= Vector(-3.145, -4, 0.528)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(10.519, -8.502, 1)
SWEP.InspectAng 					= Vector(36.583, 53.466, 34.472)

SWEP.Primary.Range 					= 16*164.042*3 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff 			= 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.CustomMuzzleFlash 				= true
SWEP.MuzzleFlashEffect 				= "sanctum2_rg_muzzle"

SWEP.TracerName 					= "effect_astw2_halo3_tracer_gauss" --Change to a string of your tracer name,or lua effect if chosen
SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.ImpactEffect 					= "simfphys_hce_snow_gauss"--Impact Effect

SWEP.TracerCount 					= 1 --0 disables, otherwise, 1 in X chance
SWEP.Secondary.ScopeZoom			= 6
SWEP.Scoped 						= true
SWEP.ScopeScale = 0
SWEP.BoltAction 					= false
SWEP.RTMaterialOverride = 0
SWEP.ScopeAngleTransforms = {
	{"R",0}, --Pitch
	{"Y",0}, --Yaw
	{"P",0}, --Roll
}
SWEP.ScopeOverlayTransforms 		= {0, 0}
SWEP.ScopeOverlayTransformMultiplier = 0.8
SWEP.ScopeShadow 					= nil
SWEP.ScopeDirt 						= nil
SWEP.ScopeReticule_CrossCol 		= false
SWEP.ScopeReticule_Scale 			= {0, 0}

SWEP.LuaShellEject 					= true
SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(0.075,-3,0.00)
SWEP.Blowback_Shell_Effect 			= "ShellEject"

SWEP.ThirdPersonReloadDisable		= false



SWEP.Blowback_PistolMode 			= false
SWEP.BlowbackBoneMods 				= {
	--["Bolt"] = { scale = Vector(1, 1, 1), pos = Vector(-3.537, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.Offset = { --Procedural world model animation, defaulted for CS:S purposes.
        Pos = {
        Up = -4,
        Right = 1,
        Forward = 2,
        },
        Ang = {
        Up = -1,
        Right = 5,
        Forward = 178
        },
		Scale = 1.0
}

SWEP.StatCache_Blacklist = {

	["Primary.Damage"] = true,
	["Primary.DamageType"] = true,
	["Primary.ClipSize"] = true,
	["Primary.HullSize"] = true,
	["Primary.KickUp"]	= true,
	["Primary.KickDown"] = true, 
	["Primary.Sound"] = true,
	["Primary.ReloadSound"] = true,
	["Primary.PenetrationMultiplier"] = true

}

function SWEP:Precache() 
 	util.PrecacheSound( self.ShootSound ) 
 end 

SWEP.CustomBulletCallback = function(attacker, trace, dmginfo)
	local wep = attacker:GetActiveWeapon()
	//attacker:PrintMessage(HUD_PRINTTALK, wep.Primary_TFA.Damage)
	if not IsValid(wep) or wep.ClassName ~= "chaosnarwhal_m99" then return end
	if not trace.HitPos then return end
	if wep:GetNWBool("m99_charged_attack") == true then
		wep:SetNWBool("m99_charged_attack", false)
	end
end


DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

function SWEP:SetupDataTables(...)
	self:SetNWBool("m99_charged_attack", false)
	self:SetNWInt("m99_charged_attack_times", 0)
	return BaseClass.SetupDataTables(self, ...)
end

local CurrentTimer = 0

function SWEP:Think2(...) -- We're overriding Think2 without touching the main think function, which is called from there anyway
	if (SERVER or CLIENT) and IsFirstTimePredicted() then
		if self:GetOwner():KeyDown(IN_RELOAD) and self:GetStatus() == TFA.Enum.STATUS_IDLE and self:Clip1() != 1 then
			if (CurrentTimer == 35) then
				if self:Clip1() == 0 then
					self:SetClip1(4)
				end

				self:SetNWInt("m99_charged_attack_times", self:Clip1())
				
				if self:GetNWBool("m99_charged_attack", true) then
					self:SetNWBool("m99_charged_attack", false)
					self.Primary_TFA.ClipSize = 4
					self.Primary_TFA.PenetrationMultiplier = 1
					self.Primary_TFA.HullSize = 4
					self:SetClip1(0)
					self:Reload(true)
				else
					self:SetNWBool("m99_charged_attack", true)
					self.Primary_TFA.ClipSize = 1
					self.Primary_TFA.PenetrationMultiplier = .2
					self.Primary_TFA.HullSize = 10
					self:SetClip1(0)
					self:Reload(true)
				end

			end
			if (CurrentTimer >= 131) then
				//self.Owner:PrintMessage(HUD_PRINTTALK, tostring(self.Primary_TFA.Damage))\
				//self:SetNWBool("m99_charged_attack", true)
			else
				CurrentTimer = CurrentTimer + 1
			end
		end
		if not self:GetOwner():KeyDown(IN_RELOAD) then
			CurrentTimer = 0
		end
	end
	
	
	
	if self:GetNWBool("m99_charged_attack") == true then
		if self:GetNWInt("m99_charged_attack_times") >= 4 then
			self.Primary_TFA.Sound = Sound ("gauss_fire")
		end
		self.Primary_TFA.Damage = self.Primary.DamageBackup*(self:GetNWInt("m99_charged_attack_times")*0.15)
		self.Primary_TFA.DamageType = DMG_BLAST
		self.Primary_TFA.ClipSize = 1
		self.Primary_TFA.HullSize = 4
		self.Primary_TFA.KickUp = 0
		self.Primary_TFA.KickDown = 0
	else
		self.Primary_TFA.Sound = Sound ("TFA_IZANAGI_FIRE.1")
		self.Primary_TFA.DamageType = DMG_BULLET
		self.Primary_TFA.ClipSize = 4
		self.Primary_TFA.HullSize = 4
		self.Primary_TFA.KickUp = 0 
		self.Primary_TFA.KickDown = 0 
		self.Primary_TFA.Damage = self.Primary.DamageBackup
	end

	if self.Owner:KeyPressed(IN_ATTACK2) and self:GetIronSights()==true then
		self.Weapon:EmitSound("vuthakral/halo/weapons/srs99c/zoom_in.wav")
	end
	
	if self.Owner:KeyReleased(IN_ATTACK2) and self:GetIronSights()==false then
		self.Weapon:EmitSound("vuthakral/halo/weapons/srs99c/zoom_out.wav")
	end

    local ViewModel = self.Owner:GetViewModel( )
 
    local compassAng = (self.Owner:GetAimVector():Angle().y)
    if compassAng > 22 and compassAng < 68      then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_nw" )
    elseif compassAng > 67 and compassAng < 113     then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_w" )
    elseif compassAng > 112 and compassAng < 157    then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_sw" )
    elseif compassAng > 156 and compassAng < 202    then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_s" )
    elseif compassAng > 201 and compassAng < 246    then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_se" )
    elseif compassAng > 245 and compassAng < 291    then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_e" )
    elseif compassAng > 290 and compassAng < 336    then
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_ne" )
    else
        ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_n" )
    end
   
    if CLIENT then
        if self:IsCarriedByLocalPlayer() == false then
            ViewModel:SetSubMaterial( 7, "" )
        end
    end
 
    local ammoString = tostring( self:Clip1() )
    local ammoOnes = string.Right( ammoString, 1 )
    local ammoTens = string.Left( ammoString, 1 )
   
    if self:Clip1() < 10 then
        ammoTens = "0"
    end
   
    self.Bodygroups_V[8] =  tonumber( ammoTens )
    self.Bodygroups_V[7] =  tonumber( ammoOnes )   
    self.Bodygroups_W[8] =  tonumber( ammoTens )
    self.Bodygroups_W[7] =  tonumber( ammoOnes )
	
	return BaseClass.Think2(self, ...)
	
end

function SWEP:PostReload(released)
	if released and self:Clip1() == 1 and self:GetNWBool("m99_charged_attack") == true then
		--self.Owner:PrintMessage(HUD_PRINTTALK, "Yes")
		self:SetClip1(0)
		self:Reload(true)
		self:SetNWBool("m99_charged_attack", false)
	end
end

function SWEP:Deploy(...)

if SERVER then

    local ply = self:GetOwner()
    
    local AuthorizedUser = AuthorizedUser or {}

    local AuthorizedUser = {
			["Spartan Command Staff"] = true,
			["Xerxes"] = true,
			["Eden"] = true,
			["Sigma"] = true,
			["Eclipse"] = true,
			["Warden"] = true,
			["Nexus"] = true
    }

    local plyteam = ply:getJobTable().category

    if not AuthorizedUser[plyteam] then
        ply:StripWeapon("chaosnarwhal_m99")
    end

end

	if self:GetNWBool("m99_charged_attack") == false then
		self:SetNWInt("m99_charged_attack_times", 0)
		self:SetNWBool("m99_charged_attack", false)
	end
	return BaseClass.Deploy(self, ...)
end
function SWEP:Holster(...)
	if self:GetNWBool("m99_charged_attack") == false then
		self:SetNWInt("m99_charged_attack_times", 0)
		self:SetNWBool("m99_charged_attack", false)
	end
	return BaseClass.Holster(self, ...)
end
function SWEP:OnRemove(...)
	self:SetNWInt("m99_charged_attack_times", 0)
	self:SetNWBool("m99_charged_attack", false)
	return BaseClass.Holster(self, ...)
end

if CLIENT then

function SWEP:DetectionHUD(trace)

    if trace.Entity and trace.Entity:IsNPC() || trace.Entity:IsPlayer() then
    color = Color(255,0,0,255)

    else
    color = Color( 0, 161, 255, 255 )
    end
    
    surface.SetTexture(surface.GetTextureID("models/vuthakral/halo/HUD/reticles/ret_sr"))
    surface.SetDrawColor( color )
    surface.DrawTexturedRect( ScrW()/2-8, ScrH()/2+44 - 50, 15, 15 )

end



function SWEP:DrawHUD()

    if self.Owner:InVehicle() then return end

    local Trace = {}
    Trace.start = self.Owner:GetShootPos()
    Trace.endpos = Trace.start + (self.Owner:GetAimVector() * 1800)
    Trace.filter = { self.Owner, self.Weapon, 0 }
    Trace.mask = MASK_SHOT
    local tr = util.TraceLine(Trace)
    
    self:DetectionHUD(tr)  

    self.CLOldNearWallProgress = self.CLOldNearWallProgress or 0
    cam.Start3D() --Workaround for vec:ToScreen()
    cam.End3D()

    self:DoInspectionDerma()

    self:DrawHUDAmmo()

    if self.IronSightsProgress > self.ScopeOverlayThreshold then

    if self.SightsDown == false then return end

    local Scoped        = true
    local ScopeMat      = "models/vuthakral/halo/HUD/scope_sniper.png"
    local ScopeBlur     = true
    local ScopeBGCol    = Color(0, 0, 0, 200)
    local IronFOV       = 200
    local ScopeScale    = 0.65
    local ScopeWidth    = 1.75
    local ScopeHeight   = 1
    local ScopeYOffset = -1

    local w = ScrW()
    local h = ScrH()
    
    local ratio = w/h
    
    local ss = 4 * ScopeScale
    local sw = ScopeWidth
    local sh = ScopeHeight
    
    local wi = w / 10 * ss
    local hi = h / 10 * ss
    
    local Q1Mat = ScopeMat
    local Q2Mat = Q2Mat
    local Q3Mat = Q3Mat
    local Q4Mat = Q4Mat
    
    local YOffset = -ScopeYOffset
    
    surface.SetDrawColor( ScopeBGCol )
    
    surface.DrawRect( 0, (h/2 - hi * sh) * YOffset, w/2 - hi / 2 * sw * 2, hi * 2 )
    surface.DrawRect( w/2 + hi * sw, (h/2 - hi * sh) * YOffset, w/2 + wi * sw, hi * 2 )
    surface.DrawRect( 0, 0, w * ss, h / 2 - hi * sh )
    surface.DrawRect( 0, (h/2 + hi * sh) * YOffset, w * ss, h / 1.99 - hi * sh )
    
    if ScopeCol != nil then
        surface.SetDrawColor( ScopeCol )
    else
        surface.SetDrawColor( Color(0, 0, 0, 255) )
    end
    
    if Q1Mat == nil then
        surface.SetMaterial(Material("sprites/scope_arc"))
    else 
        surface.SetMaterial(Material(Q1Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 1, 1, 0, 0 )
    
    if Q2Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q2Mat))
    end
    surface.DrawTexturedRectUV( w / 2, (h/2 - hi) * YOffset, hi * sw, hi * sh, 0, 1, 1, 0 )
    
    if Q3Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q3Mat))
    end
    surface.DrawTexturedRectUV( w/2 - hi / 2 * sw * 2, h/2, hi * sw, hi * sh, 1, 0, 0, 1 )
    
    if Q4Mat == nil then
        if Q1Mat == nil then
            surface.SetMaterial(Material("sprites/scope_arc"))
        else
            surface.SetMaterial(Material(Q1Mat))
        end
    else 
        surface.SetMaterial(Material(Q4Mat))
    end
    surface.DrawTexturedRectUV( w/2, h/2, hi * sw, hi * sh, 0, 0, 1, 1 )

    end

end




end

