if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M7S Reflex Sight"
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Reflex Sight & Ammo counter", TFA.AttachmentColors["+"], "Quick target acquisition"}
ATTACHMENT.Icon = "entities/m7/smg_m7reflex.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M7S Reflex"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[3] = 1
	},
	["Bodygroups_W"] = {
		[2] = 1
	},
	["IronSightsPos"] = function(wep, val) return wep.IronSightsPos_M7REFLEX or val, true end,
	["IronSightsAng"] = function(wep, val) return wep.IronSightsAng_M7REFLEX or val, true end,
	["Secondary"] = {
		["IronFOV"] = function(wep, val) return val * 0.85 end
	},
	["IronSightTime"] = function(wep, val) return val * 0.95 end,
	["IronSightsSensitivity"] = 0.25,
	["BlowbackVector"] = Vector(0.05,-3,0.00)
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end