if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M7S silencer"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Real Gs move in silence like...."}
ATTACHMENT.Icon = "entities/m7/smg_m7s_silencer.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M7S Silencer"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[4] = 2
	},
	["Bodygroups_W"] = {
		[3] = 2
	},
	["Primary"] = {
		["Sound"] = Sound("Weapon_reachishi_m7s.Single"),
		["KickUp"] = function(wep,stat) return (stat - 0.15) end,
		["StaticRecoilFactor"] = function(wep,stat) return (stat - 0.15) end
	},
	["MoveSpeed"] = 0.95,
	["MuzzleAttachmentMod"] = 2,
	["MuzzleFlashEffect"] = "tfa_muzzleflash_silenced"
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end