if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Frag Ammunition"
ATTACHMENT.ShortName = "Frag" --Abbreviation, 5 chars or less please
--ATTACHMENT.ID = "base" -- normally this is just your filename
ATTACHMENT.Description = {
	TFA.AttachmentColors["="], "This mod will replace the rounds with ",
	TFA.AttachmentColors["="], "Frag rounds to increase damage and add explosive ordinance",
	TFA.AttachmentColors["+"], "0.5% more bullet DMG",
	TFA.AttachmentColors["-"], "2 roung magazine capacity"
}

ATTACHMENT.Icon = "entities/tfa_ammo_fragshell.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"

ATTACHMENT.WeaponTable = {
	["Primary"] = {
		["DamageType"] = function(wep,stat) return bit.bor( stat or 0, DMG_BLAST ) end,
		}
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end
