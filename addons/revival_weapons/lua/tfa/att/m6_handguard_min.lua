if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series minimized handguard"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Minimized handguard for additional mobility."}
ATTACHMENT.Icon = "entities/m6/m6_handguard_min.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Handmin"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[3] = 0
	},
	["Bodygroups_W"] = {
		[3] = 0
	},
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end