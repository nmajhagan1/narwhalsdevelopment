if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6S/SOCOM framecc"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "M6S/SOCOM frame w/ Integrated suppressor & VnSLS/V 6E SL optics"}
ATTACHMENT.Icon = "entities/m6/m6s_body_comp.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6S"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[1] = 2
	},
	["Bodygroups_W"] = {
		[1] = 2
	},
	
	
	["VElements"] = {
		["silencer"] = {
			["active"] = false
		},
		["laser"] = {
			["active"] = false
		},
		["comp"] = {
			["active"] = true
		}
	},
	["WElements"] = {
		["silencer"] = {
			["active"] = false
		},
		["laser"] = {
			["active"] = false
		},
		["comp"] = {
			["active"] = true
		}
	},

	["Primary"] = { 
	["Sound"] = Sound("Weapon_reachishi_m6_comp.Single"),
	["MuzzleAttachmentMod"] = 2,
	["MuzzleFlashEffect"] = "tfa_muzzleflash_silenced",
	["Damage"] = 105,
	["KickUp"] = 0.1,
	["StaticRecoilFactor"] = 0.1,
	["ClipSize"] = 12,
	},
	
	["IronSightsPos"] = function(wep, val) return wep.IronSightsPos_COMP or val, true end,
	["IronSightsAng"] = function(wep, val) return wep.IronSightsAng_COMP or val, true end,
	["Secondary"] = {
		["IronFOV"] = function(wep, val) return val * 0.85 end
	},
	["IronSightTime"] = function(wep, val) return val * 1.15 end,
	["IronSightsSensitivity"] = 0.3,
	["BlowbackVector"] = Vector(0.075,-3,0.00),
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end