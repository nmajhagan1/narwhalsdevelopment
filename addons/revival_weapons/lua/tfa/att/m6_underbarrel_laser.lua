if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6-series laser module"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Laser targeting module."}
ATTACHMENT.Icon = "entities/m6/m6_underbarrel_light.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6 Laser"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[2] = 1
	},
	["Bodygroups_W"] = {
		[2] = 1
	},

	["VElements"] = {
		["laser"] = {
			["active"] = true
		}
	},
	["WElements"] = {
		["laser"] = {
			["active"] = true
		}
	},
	["Primary"] = {
		["Spread"] = function(wep,stat) return (stat * 0.6) end,
		["SpreadMultiplierMax"] = function(wep,stat) return stat * ( 1 / 0.8 ) * 1.1 end
	},
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end

