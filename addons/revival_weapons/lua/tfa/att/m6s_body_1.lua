if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "M6C standard frame"
ATTACHMENT.Description = {TFA.AttachmentColors["="], "Standard chrome/nickle finish M6C sidearm frame"}
ATTACHMENT.Icon = "entities/m6/m6s_body_default.png" --Revers to label, please give it an icon though!  This should be the path to a png, like "entities/tfa_ammo_match.png"
ATTACHMENT.ShortName = "M6C"

ATTACHMENT.WeaponTable = {
	["Bodygroups_V"] = {
		[1] = 0
	},
	["Bodygroups_W"] = {
		[1] = 0
	},
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end