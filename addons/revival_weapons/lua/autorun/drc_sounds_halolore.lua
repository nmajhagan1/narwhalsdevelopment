AddCSLuaFile()

sound.Add( {
	name = "drc.m247_fire",
	channel = CHAN_AUTO,
	volume = 1,
	level = 90,
	pitch = { 99.5, 100.5 },
	sound = { "vuthakral/halo/weapons/m247/fire0.wav",
	"vuthakral/halo/weapons/m247/fire1.wav",
	"vuthakral/halo/weapons/m247/fire2.wav" }
} )

sound.Add( {
	name = "drc.M247_reload",
	channel = CHAN_AUTO,
	volume = 0.78,
	level = 42,
	pitch = { 104.5, 105.5 },
	sound = { "vuthakral/halo/weapons/m247/reload.wav" }
} )

sound.Add( {
	name = "drc.rukt_impact",
	channel = CHAN_WEAPON,
	volume = 0.72,
	level = 90,
	pitch = { 98.5, 101.5 },
	sound = { "vuthakral/halo/weapons/rukt/tartarus_hit1.wav", 
	"vuthakral/halo/weapons/rukt/tartarus_hit2.wav", 
	"vuthakral/halo/weapons/rukt/tartarus_hit3.wav", 
	"vuthakral/halo/weapons/rukt/tartarus_hit4.wav", 
	"vuthakral/halo/weapons/rukt/tartarus_hit5.wav" }
} )

sound.Add( {
	name = "drc.rukt_lunge",
	channel = CHAN_WEAPON,
	volume = 0.85,
	level = 50,
	pitch = { 99, 101 },
	sound = { "vuthakral/halo/weapons/rukt/tartarus_melee1.wav",
	"vuthakral/halo/weapons/rukt/tartarus_melee2.wav",
	"vuthakral/halo/weapons/rukt/tartarus_melee3.wav",
	"vuthakral/halo/weapons/rukt/tartarus_melee4.wav",
	"vuthakral/halo/weapons/rukt/tartarus_melee5.wav",
	"vuthakral/halo/weapons/rukt/tartarus_melee6.wav" }
} )

sound.Add( {
	name = "drc.c12_explode",
	channel = CHAN_AUTO,
	volume = 1,
	level = 120,
	pitch = { 90, 100 },
	sound = { "vuthakral/halo/weapons/c12/explode.wav" }
} )

sound.Add( {
	name = "drc.c12_explode_dist",
	channel = CHAN_AUTO,
	volume = 1.2,
	level = 140,
	pitch = { 95, 105 },
	sound = { "vuthakral/halo/weapons/c12/e_far1.wav",
	"vuthakral/halo/weapons/c12/e_far2.wav",
	"vuthakral/halo/weapons/c12/e_far3.wav" }
} )

sound.Add( {
	name = "drc.c12_beep",
	channel = CHAN_AUTO,
	volume = 0.85,
	level = 50,
	pitch = { 100, 100 },
	sound = { "vuthakral/halo/weapons/c12/beep.wav" }
} )