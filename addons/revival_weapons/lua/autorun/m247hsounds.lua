sound.Add( {
	name = "Weapon_chaosnarwhal_m247h.Single",
	channel = CHAN_WEAPON,
	volume = 1.0,
	level = SNDLVL_GUNFIRE,
	pitch = {100,105},
	sound = {
		"chaosnarwhal/weapons/m247h/gunfire/machinegun_fire_1.wav",
		"chaosnarwhal/weapons/m247h/gunfire/machinegun_fire_2.wav",
		"chaosnarwhal/weapons/m247h/gunfire/machinegun_fire_3.wav",
		"chaosnarwhal/weapons/m247h/gunfire/machinegun_fire_4.wav",
 	},
})