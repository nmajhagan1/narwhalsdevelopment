

AddCSLuaFile()   



function EFFECT:Init(data)
self.Entity 				= data:GetEntity()				--Entity determines what is creating the dynamic light			
self.Pos 					= data:GetOrigin()				--Origin determines the global position of the effect			
self.Scale 					= 1							-- Scale determines how large the effect is			
self.Radius 				= data:GetRadius() or 1			--Radius determines what type of effect to create, default is Concrete	
self.DirVec 				= data:GetNormal()				--Normal determines the direction of impact for the effect	
self.PenVec 				= data:GetStart()				--PenVec determines the direction of the round for penetrations
self.Particles 				= data:GetMagnitude()			--Particles determines how many puffs to make, primarily for "trails"
self.Angle 					= self.DirVec:Angle()			--Angle is the angle of impact from Normal			
self.DebrizzlemyNizzle 		= 10+data:GetScale()			--Debrizzle my Nizzle is how many "trails" to make		
self.Size 					= 5*self.Scale					--Size is exclusively for the explosion "trails" size
self.Emitter 				= ParticleEmitter( self.Pos )	--Emitter must be there so you don't get an error	

		for i=1,5 do 
		local Flash = self.Emitter:Add( "effects/muzzleflash"..math.random(1,4), self.Pos )
		if (Flash) then
		Flash:SetVelocity( self.DirVec*100 )
		Flash:SetAirResistance( 200 )
		Flash:SetDieTime( 0.15 )
		Flash:SetStartAlpha( 255 )
		Flash:SetEndAlpha( 0 )
		Flash:SetStartSize( self.Scale*300 )
		Flash:SetEndSize( 0 )
		Flash:SetRoll( math.Rand(180,480) )
		Flash:SetRollDelta( math.Rand(-1,1) )
		Flash:SetColor(255,255,255)	
		end
		end

		local Distort = self.Emitter:Add( "sprites/heatwave", self.Pos )
		if (Distort) then
		Distort:SetVelocity( self.DirVec )
		Distort:SetAirResistance( 200 )
		Distort:SetDieTime( 0.1 )
		Distort:SetStartAlpha( 255 )
		Distort:SetEndAlpha( 0 )
		Distort:SetStartSize( self.Scale*600 )
		Distort:SetEndSize( 0 )
		Distort:SetRoll( math.Rand(180,480) )
		Distort:SetRollDelta( math.Rand(-1,1) )
		Distort:SetColor(255,255,255)	
		end

		for i=1, 20*self.Scale do
		local Dust = self.Emitter:Add( "particle/particle_composite", self.Pos )	
		if (Dust) then
		Dust:SetVelocity( self.DirVec * math.random( 100,400)*self.Scale + ((VectorRand():GetNormalized()*300)*self.Scale) )
		Dust:SetDieTime( math.Rand( 2 , 3 ) )
		Dust:SetStartAlpha( 230 )
		Dust:SetEndAlpha( 0 )
		Dust:SetStartSize( (50*self.Scale) )
		Dust:SetEndSize( (100*self.Scale) )
		Dust:SetRoll( math.Rand(150, 360) )
		Dust:SetRollDelta( math.Rand(-1, 1) )			
		Dust:SetAirResistance( 150 ) 			 
		Dust:SetGravity( Vector( 0, 0, math.Rand(-100, -400) ) ) 			
		Dust:SetColor( 20,20,20 )
		end
		end

		for i=1, 15*self.Scale do
		local Dust = self.Emitter:Add( "particle/smokesprites_000"..math.random(1,9), self.Pos )
		if (Dust) then
		Dust:SetVelocity( self.DirVec * math.random( 100,400)*self.Scale + ((VectorRand():GetNormalized()*400)*self.Scale) )
		Dust:SetDieTime( math.Rand( 1 , 5 )*self.Scale )
		Dust:SetStartAlpha( 50 )
		Dust:SetEndAlpha( 0 )
		Dust:SetStartSize( (80*self.Scale) )
		Dust:SetEndSize( (100*self.Scale) )
		Dust:SetRoll( math.Rand(150, 360) )
		Dust:SetRollDelta( math.Rand(-1, 1) )			
		Dust:SetAirResistance( 250 ) 			 
		Dust:SetGravity( Vector( math.Rand( -200 , 200 ), math.Rand( -200 , 200 ), math.Rand( 10 , 100 ) ) )		
		Dust:SetColor( 90,83,68 )
		end
		end

		for i=1, 25*self.Scale do
		local Debris = self.Emitter:Add( "effects/fleck_cement"..math.random(1,2), self.Pos )
		if (Debris) then
		Debris:SetVelocity ( self.DirVec * math.random(0,500)*self.Scale + VectorRand():GetNormalized() * math.random(0,400)*self.Scale )
		Debris:SetDieTime( math.random( 1, 2) * self.Scale )
		Debris:SetStartAlpha( 255 )
		Debris:SetEndAlpha( 0 )
		Debris:SetStartSize( math.random(5,10)*self.Scale)
		Debris:SetRoll( math.Rand(0, 360) )
		Debris:SetRollDelta( math.Rand(-5, 5) )			
		Debris:SetAirResistance( 40 ) 			 			
		Debris:SetColor( 50,53,45 )
		Debris:SetGravity( Vector( 0, 0, -600) ) 	
		end
		end

		local Angle = self.DirVec:Angle()
		for i = 1, self.DebrizzlemyNizzle do 					/// This part makes the trailers ///
		Angle:RotateAroundAxis(Angle:Forward(), (360/self.DebrizzlemyNizzle))
		local DustRing = Angle:Up()
		local RanVec = self.DirVec*math.Rand(2, 6) + (DustRing*math.Rand(1, 4))	

			for k = 3, self.Particles do
			local Rcolor = math.random(-20,20)
			local particle1 = self.Emitter:Add( "particle/smokesprites_000"..math.random(1,9), self.Pos )				
			particle1:SetVelocity((VectorRand():GetNormalized()*math.Rand(1, 2) * self.Size) + (RanVec*self.Size*k*3.5))	
			particle1:SetDieTime( math.Rand( 0.5, 4 )*self.Scale )	
			particle1:SetStartAlpha( math.Rand( 90, 100 ) )			
			particle1:SetEndAlpha(0)	
			particle1:SetGravity((VectorRand():GetNormalized()*math.Rand(5, 10)* self.Size) + Vector(0,0,-50))
			particle1:SetAirResistance( 200+self.Scale*20 ) 		
			particle1:SetStartSize( (5*self.Size)-((k/self.Particles)*self.Size*3) )	
			particle1:SetEndSize( (20*self.Size)-((k/self.Particles)*self.Size) )
			particle1:SetRoll( math.random( -500, 500 )/100 )	
			particle1:SetRollDelta( math.random( -0.5, 0.5 ) )	
			particle1:SetColor( 90+Rcolor,83+Rcolor,68+Rcolor )
			end
		end
	end

function EFFECT:Think( )	return false	end

function EFFECT:Render()	end