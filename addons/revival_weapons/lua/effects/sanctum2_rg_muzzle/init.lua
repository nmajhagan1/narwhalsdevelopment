function EFFECT:Init(data)
	
	if not IsValid(data:GetEntity()) then return end
	if not IsValid(data:GetEntity():GetOwner()) then return end
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	
	if self.WeaponEnt == nil or self.WeaponEnt:GetOwner() == nil or self.WeaponEnt:GetOwner():GetVelocity() == nil then 
		return
	else
	
	self.Position = self:GetTracerShootPos(data:GetOrigin(), self.WeaponEnt, self.Attachment)
	self.Forward = data:GetNormal()
	self.Angle = self.Forward:Angle() + Angle(0,0,45)
	self.Right = self.Angle:Right()
	self.Up = self.Angle:Up()
	
	local AddVel = self.WeaponEnt:GetOwner():GetVelocity()
	
	local emitter = ParticleEmitter(self.Position)
	if emitter != nil then	


		for j = 1,2 do

			for i = -1,1,2 do 

				local particle = emitter:Add( "effects/muzzleflash"..math.random( 1, 4 ), self.Position - 3 * self.Forward + 2 * j * i * self.Right)

					particle:SetVelocity( 60 * j * i * self.Right + AddVel )
					particle:SetGravity( AddVel )

					particle:SetDieTime( 0.1 )

					particle:SetStartAlpha( 159 )

					particle:SetStartSize( j )
					particle:SetEndSize( 4 * j )

					particle:SetRoll( math.Rand( 180, 480 ) )
					particle:SetRollDelta( math.Rand( -1, 1 ) )

					particle:SetColor( 255, 255, 255 )	
			end
		end
			
		for j = 1,2 do

			for i = -1,1,2 do 

				local particle = emitter:Add( "effects/muzzleflash"..math.random( 1, 4 ), self.Position - 3 * self.Forward + 2 * j * i * self.Up)

					particle:SetVelocity( 60 * j * i * self.Up + AddVel )
					particle:SetGravity( AddVel )

					particle:SetDieTime( 0.1 )

					particle:SetStartAlpha( 159 )

					particle:SetStartSize( j )
					particle:SetEndSize( 4 * j )

					particle:SetRoll( math.Rand( 180, 480 ) )
					particle:SetRollDelta( math.Rand( -1, 1 ) )

					particle:SetColor( 255, 255, 255 )	
			end
		end

		for i = 1,2 do 

			local particle = emitter:Add( "effects/muzzleflash"..math.random( 1, 4 ), self.Position + 8 * self.Forward )

				particle:SetVelocity( 350 * self.Forward + 1.1 * AddVel )
				particle:SetAirResistance( 160 )

				particle:SetDieTime( 0.1 )

				particle:SetStartAlpha( 159 )
				particle:SetEndAlpha( 0 )

				particle:SetStartSize( 6 * i )
				particle:SetEndSize( 5 * i )

				particle:SetRoll( math.Rand( 180, 480 ) )
				particle:SetRollDelta( math.Rand( -1, 1) )

				particle:SetColor( 255, 255, 255 )	
			end

	end
	emitter:Finish()
	end
end


function EFFECT:Think()

	return false
end


function EFFECT:Render()
end