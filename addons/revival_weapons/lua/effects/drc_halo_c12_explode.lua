function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )

	for i = 1,97 do
		local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle) then
			particle:SetVelocity( i * Vector(math.random(-0.1,0.1),math.random(-0.1,0.1),math.random(0,0)))
			particle:SetLifeTime(0) 
			particle:SetDieTime(i * 0.017) 
			particle:SetStartAlpha(math.Rand(100,255))
			particle:SetEndAlpha(0)
			particle:SetStartSize(60) 
			particle:SetEndSize( i * math.Rand(14,49))
			particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle:SetAngleVelocity( Angle(math.Rand(12,15)) ) 
			particle:SetRoll(math.Rand( 0, 360 ))
			particle:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle:SetGravity( Vector(0,0,0) ) 
			particle:SetAirResistance(-68.167394537726 )  
			particle:SetCollide(true)
			particle:SetBounce(0.1419790559388)
		end
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,72 do
		local particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * i * 5):GetNormal() * math.Rand(95, 295));
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(2,7)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(7) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle2:SetGravity( Vector(0,0,-400) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
		end
	end
	
	local emitter3 = ParticleEmitter( Pos )
	
	for i = 1,295 do
		local particle3 = emitter2:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle3 == nil then particle3 = emitter2:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle3) then
			particle3:SetVelocity( i * Vector(math.random(-5,5),math.random(-5,5),math.random(0,0)))
			particle3:SetLifeTime(math.Rand(5, 15)) 
			particle3:SetDieTime(15) 
			particle3:SetStartAlpha(math.Rand(200,255))
			particle3:SetEndAlpha(0)
			particle3:SetStartSize(i * math.Rand(0.1, 1)) 
			particle3:SetEndSize(math.Rand(1000,1500))
			particle3:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle3:SetRoll(math.Rand( 0, 360 ))
			particle3:SetColor(0, 0, 0)
			particle3:SetGravity( Vector(0,0,i * 3) ) 
			particle3:SetAirResistance(0.15)  
			particle3:SetCollide(true)
			particle3:SetBounce(0)
		end
	end
	
local emitter4 = ParticleEmitter( Pos )
	
	for i = 1,math.Rand(90, 300) do
		local particle4 = emitter2:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle4 == nil then particle4 = emitter2:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle4) then
			particle4:SetVelocity((-self.Normal+VectorRand() * i * math.Rand(105,405)):GetNormal() * math.Rand(105, 1605));
			particle4:SetLifeTime(math.Rand(5, 35)) 
			particle4:SetDieTime(math.Rand(5,35)) 
			particle4:SetStartAlpha(255)
			particle4:SetEndAlpha(0)
			particle4:SetLighting(1)
			particle4:SetStartSize(math.Rand(106,342)) 
			particle4:SetEndSize(math.Rand(102,1987))
			particle4:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle4:SetRoll(math.Rand( 0, 360 ))
			particle4:SetColor(60, 60, 60)
			particle4:SetGravity( Vector(0,0,100) ) 
			particle4:SetAirResistance(0.1)  
			particle4:SetCollide(true)
			particle4:SetBounce(0)
		end
	end
	
local emitter5 = ParticleEmitter ( Pos )

	for i = 1,38 do
		local particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle5 == nil then particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle5) then
			particle5:SetVelocity((-self.Normal+VectorRand() * math.Rand(15,45)):GetNormal() * math.Rand(305, 965));
			particle5:SetLifeTime(0) 
			particle5:SetDieTime(0.4) 
			particle5:SetStartAlpha(255)
			particle5:SetEndAlpha(0)
			particle5:SetStartSize(5) 
			particle5:SetEndSize(0)
			particle5:SetStartLength(100)
			particle5:SetEndLength(0)
			particle5:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle5:SetAngleVelocity( Angle(0) ) 
			particle5:SetRoll(0)
			particle5:SetColor(math.random(200,220),math.random(90,100),math.random(47,83),math.random(180,255))
			particle5:SetGravity( Vector(0,0,0) ) 
			particle5:SetAirResistance( 0.5 )  
			particle5:SetCollide(true)
			particle5:SetBounce(0.1419790559388)
		end
	end
	
local emitter6 = ParticleEmitter ( Pos )
	
	for i = 1,400 do
		local particle3 = emitter6:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle3 == nil then particle3 = emitter6:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle3) then
			particle3:SetVelocity( i * Vector(math.random(-45,45),math.random(-45,45),math.random(0,0)))
			particle3:SetLifeTime(math.Rand(15, 35)) 
			particle3:SetDieTime(35) 
			particle3:SetStartAlpha(math.Rand(200,255))
			particle3:SetEndAlpha(0)
			particle3:SetStartSize(i * math.Rand(0.1, 1)) 
			particle3:SetEndSize(math.Rand(250,600))
			particle3:SetAngleVelocity( Angle(1.2934407040912,4.149586106307,0.18606363772742) ) 
			particle3:SetRoll(math.Rand( 0, 25 ))
			particle3:SetColor(math.Rand(90,120), math.Rand(90, 120), math.Rand(90,120))
			particle3:SetGravity( Vector(math.Rand(800, -800),math.Rand(800, -800),-100) ) 
			particle3:SetAirResistance(0.15)  
			particle3:SetCollide(true)
			particle3:SetBounce(0)
		end
	end
	
local emitter7 = ParticleEmitter ( Pos )
	
	for i = 1,400 do
		local particle3 = emitter7:Add( "particle/particle_smokegrenade", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle3 == nil then particle3 = emitter7:Add( "particle/particle_smokegrenade", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		if (particle3) then
			particle3:SetVelocity( i * Vector(math.random(-5,5),math.random(-5,5),math.random(0,0)))
			particle3:SetLifeTime(math.Rand(15, 35)) 
			particle3:SetDieTime(35) 
			particle3:SetStartAlpha(math.Rand(200,255))
			particle3:SetEndAlpha(0)
			particle3:SetStartSize(i * math.Rand(0.1, 1)) 
			particle3:SetEndSize(math.Rand(250,600))
			particle3:SetAngleVelocity( Angle(1.2934407040912,4.149586106307,0.18606363772742) ) 
			particle3:SetRoll(math.Rand( 0, 25 ))
			particle3:SetColor(math.Rand(90,120), math.Rand(90, 120), math.Rand(90,120))
			particle3:SetGravity( Vector(math.Rand(45, -45),math.Rand(45, -45),0) ) 
			particle3:SetAirResistance(0.15)  
			particle3:SetCollide(false)
			particle3:SetBounce(0)
		end
	end

	emitter:Finish()
	emitter2:Finish()
	emitter3:Finish()
	emitter4:Finish()
	emitter5:Finish()
	emitter6:Finish()
	emitter7:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

