EFFECT.Mat = Material( "trails/laser" )
EFFECT.Mat2 = Material("trails/laser")


function EFFECT:Init( data )

	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()

	-- Keep the start and end pos - we're going to interpolate between them
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()

	self.Alpha = 255
	self.Life = 0.001

	local Pos = self.StartPos
	
	
	local emitter = ParticleEmitter( Pos )
	
	for i = 1,9 do

		local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		 
		if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		
		if (particle) then
			particle:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle:SetLifeTime(0) 
			particle:SetDieTime(.2) 
			particle:SetStartAlpha(255)
			particle:SetEndAlpha(0)
			particle:SetStartSize(math.Rand(5, 15)) 
			particle:SetEndSize(math.Rand(1, 25))
			particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle:SetAngleVelocity( Angle(15) ) 
			particle:SetRoll(math.Rand( 0, 360 ))
			particle:SetColor(255, 0, 0)
			particle:SetGravity( Vector(0,0,0) ) 
			particle:SetAirResistance(-68.167394537726 )  
			particle:SetCollide(true)
			particle:SetBounce(0.1419790559388)
		end
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,14 do

		local particle2 = emitter2:Add( "effects/draconic_halo/split_beam", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) 
		 
		if particle2 == nil then particle2 = emitter2:Add( "effects/draconic_halo/split_beam", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		
		if (particle2) then
			particle2:SetVelocity(Vector(math.random(-4,4),math.random(-4,4),math.random(0,0)))
			particle2:SetLifeTime(0) 
			particle2:SetDieTime(.25) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(1.5870467639842) 
			particle2:SetEndSize(25.012735977598182)
			particle2:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle2:SetAngleVelocity( Angle(15) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(math.Rand(100, 255), 0, 0)
			particle2:SetGravity( Vector(0,0,0) ) 
			particle2:SetAirResistance(-68.167394537726 )  
			particle2:SetCollide(true)
			particle2:SetBounce(0.1419790559388)
		end
	end
	
	local emitter3 = ParticleEmitter( Pos )
	
	for i = 1,8 do

		local particle3 = emitter3:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		 
		if particle3 == nil then particle3 = emitter3:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		
		if (particle3) then
			particle3:SetVelocity(VectorRand() * (math.Rand(35, 225)))
			particle3:SetLifeTime(math.Rand(0.05, 0.5))
			particle3:SetDieTime(math.Rand(3, 17)) 
			particle3:SetStartAlpha(255)
			particle3:SetEndAlpha(0)
			particle3:SetStartSize(1) 
			particle3:SetEndSize(0)
			particle3:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle3:SetRoll(math.Rand( 0, 360 ))
			particle3:SetColor(math.Rand(225,255), math.Rand(50,125), 0)
			particle3:SetGravity( Vector(0,0,-400) ) 
			particle3:SetAirResistance(0)  
			particle3:SetCollide(true)
			particle3:SetBounce(0)
		end
	end

	local ImpactData = EffectData()
		ImpactData:SetOrigin(self. EndPos )
		ImpactData:SetStart( self.StartPos )
		ImpactData:SetAttachment( 1 )
		ImpactData:SetEntity( self )
	util.Effect( "drc_halo_gnr_impact", ImpactData )
	
	emitter:Finish()
	emitter2:Finish()
	emitter3:Finish()

	self:SetRenderBoundsWS( self.StartPos, self.EndPos )

end

function EFFECT:Think()

	self.Life = self.Life + FrameTime() * 4
	self.Alpha = 255 * ( 1 - self.Life )
	
	return ( self.Life < 1 )

end

function EFFECT:Render()

	if ( self.Alpha < 1 ) then return end

	render.SetMaterial( self.Mat2 )
	local texcoord = math.Rand( 0, 1 )

	local norm = (self.StartPos - self.EndPos) * self.Life

	self.Length = norm:Length()

	for i = 1, 3 do

		render.DrawBeam( self.StartPos,		-- Start
					self.EndPos,					-- End
					17,								-- Width
					texcoord,						-- Start tex coord
					texcoord + self.Length / 128,	-- End tex coord
					Color( 250, 0, 0, 255 ) )		-- Color (optional)
	end

	render.SetMaterial( self.Mat )
	render.DrawBeam( self.StartPos,
					self.EndPos,
					25,
					texcoord,
					texcoord + ( ( self.StartPos - self.EndPos ):Length() / 128 ),
					Color( 255, 255, 255, 255 * ( 0 - self.Life ) ) )

end
