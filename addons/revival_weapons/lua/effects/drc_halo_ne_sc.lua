function EFFECT:Init( data )
	local Pos = data:GetOrigin()
	
	self.Position = data:GetStart()
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	self.DataNormal = data:GetNormal()
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self.EndPos = data:GetOrigin()
	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos)
	
	local sub = self.EndPos - self.StartPos
	self.Normal = sub:GetNormal()
	
	local emitter = ParticleEmitter( Pos )
	for i = 0, 5 do
		timer.Simple(i * 0.1, function()
			for i = 1,100 do
			local emitter = ParticleEmitter( Pos )
			local particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
			
			if particle == nil then particle = emitter:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
			if (particle) then
				particle:SetVelocity(Vector(math.random(-30.5,30.5),math.random(-30.5,30.5),math.random(math.Rand(-0.5,- 156.5), math.Rand(0.5, 156.5))) )
				particle:SetLifeTime(0) 
				particle:SetDieTime(math.Rand(0.5, 0.8)) 
				particle:SetStartAlpha(math.Rand(50, 255))
				particle:SetEndAlpha(0)
				particle:SetStartSize(math.Rand(1, 5)) 
				particle:SetEndSize(math.Rand(10, 25))
				particle:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
				particle:SetAngleVelocity( Angle(45) ) 
				particle:SetRoll(math.Rand( 0, 360 ))
				particle:SetColor(math.random(200,255),math.random(90,100),math.random(200,255),math.random(180,255))
				particle:SetGravity( Vector(0,0,0) ) 
				particle:SetAirResistance(100)  
				particle:SetCollide(true)
				particle:SetBounce(0.1419790559388)
			end
			emitter:Finish()
			end
		end)
	end
	
	local emitter2 = ParticleEmitter( Pos )
	
	for i = 1,math.Rand(13, 50) do
		local particle2 = emitter2:Add( "effects/fleck_glass2", Pos + Vector( math.random(-6,6),math.random(-6,6),math.random(0,0))) 
		if particle2 == nil then particle2 = emitter2:Add( "effects/fleck_glass2", Pos + Vector(   math.random(-6,6),math.random(-6,6),math.random(0,0) ) ) end
		
		if (particle2) then
			particle2:SetVelocity((-self.Normal+VectorRand() * 5):GetNormal() * math.Rand(35, 125));
			particle2:SetLifeTime(math.Rand(0.05, 0.5)) 
			particle2:SetDieTime(math.Rand(3, 14)) 
			particle2:SetStartAlpha(255)
			particle2:SetEndAlpha(0)
			particle2:SetStartSize(math.Rand(0.3, 3)) 
			particle2:SetEndSize(0)
			particle2:SetAngleVelocity( Angle(4.2934407040912,14.149586106307,0.18606363772742) ) 
			particle2:SetRoll(math.Rand( 0, 360 ))
			particle2:SetColor(255, 0, 255)
			particle2:SetGravity( Vector(0,0,-600) ) 
			particle2:SetAirResistance(0)  
			particle2:SetCollide(true)
			particle2:SetBounce(0)
			particle2:SetLighting(false)
		end
	end

	local emitter4 = ParticleEmitter( Pos )
	for i = 0, 5 do
		timer.Simple(i * 0.1, function()
			for i = 1,5 do
			local emitter4 = ParticleEmitter( Pos )
			local particle4 = emitter4:Add( "effects/draconic_halo/flash_soft", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
			
			if particle4 == nil then particle4 = emitter4:Add( "effects/draconic_halo/flash_soft", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
			if (particle4) then
				particle4:SetVelocity(Vector(0, 0, 0))
				particle4:SetLifeTime(0) 
				particle4:SetDieTime(math.Rand(0.5, 0.8)) 
				particle4:SetStartAlpha(math.Rand(50, 255))
				particle4:SetEndAlpha(0)
				particle4:SetStartSize(math.Rand(6, 12)) 
				particle4:SetEndSize(math.Rand(36, 95))
				particle4:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
				particle4:SetAngleVelocity( Angle(45) ) 
				particle4:SetRoll(math.Rand( 0, 360 ))
				particle4:SetColor(math.random(200,255),math.random(5,20),math.random(200,255),math.random(180,255))
				particle4:SetGravity( Vector(0,0,0) ) 
				particle4:SetAirResistance(100)  
				particle4:SetCollide(true)
				particle4:SetBounce(0.1419790559388)
			end
			emitter4:Finish()
			end
		end)
	end
	
local emitter5 = ParticleEmitter ( Pos )

	for i = 1,38 do
		local particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector( math.random(0,0),math.random(0,0),math.random(0,0) ) ) 
		if particle5 == nil then particle5 = emitter5:Add( "effects/draconic_halo/flash_large", Pos + Vector(   math.random(0,0),math.random(0,0),math.random(0,0) ) ) end
		if (particle5) then
			particle5:SetVelocity((-self.Normal+VectorRand() * math.Rand(5,15)):GetNormal() * math.Rand (50, 100));
			particle5:SetLifeTime(0) 
			particle5:SetDieTime(math.Rand(0.15,0.45)) 
			particle5:SetStartAlpha(255)
			particle5:SetEndAlpha(0)
			particle5:SetStartSize(5)
			particle5:SetEndSize(0)
			particle5:SetStartLength(25)
			particle5:SetEndLength(0)
			particle5:SetAngles( Angle(21.424716258016,3.5762036133102,5.6347174018494) )
			particle5:SetAngleVelocity( Angle(0) ) 
			particle5:SetRoll(0)
			particle5:SetColor(math.random(200,255),math.random(90,100),math.random(200,255),math.random(180,255))
			particle5:SetGravity( Vector(0,0,0) ) 
			particle5:SetAirResistance( 0.5 )  
			particle5:SetCollide(true)
			particle5:SetBounce(0.1419790559388)
		end
	end

	emitter:Finish()
	emitter2:Finish()
	emitter4:Finish()
	emitter5:Finish()
		
end

function EFFECT:Think()		
	return false
end

function EFFECT:Render()
end

