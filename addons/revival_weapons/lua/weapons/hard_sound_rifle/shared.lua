SWEP.Author                         = "Void"
SWEP.PrintName                      = "Hard Sound Rifle"
SWEP.Purpose                        = "FOR SILENT ASSASSINATIONS"
SWEP.Base                           = "tfa_bash_base"
SWEP.Spawnable                      = false
SWEP.AdminSpawnable                 = true

SWEP.Slot                           = 4
SWEP.SlotPos                        = 5

SWEP.Shotgun 						= false
SWEP.ShellTime 						= 0.75

local ShootSound = Sound("garrysmod/sound/sound_rifle.wav")

SWEP.DisableChambering 				= true
SWEP.ViewModel						= "models/chaosnarwhal/oni/weapons/v_hardsound_rifle.mdl"
SWEP.WorldModel						= "models/chaosnarwhal/oni/weapons/w_hardsound_rifle.mdl"
SWEP.ViewModelFOV 					= 70
SWEP.HoldType 						= "ar2"
SWEP.ViewMOdelFOV                   = 900
SWEP.Primary.RPM 					= 125
SWEP.Primary.Damage                 = 3000
SWEP.Primary.ClipSize               = 3
SWEP.Primary.DefaultClip            = 30
SWEP.Primary.Sound                  = "sound_rifle.wav"
SWEP.Primary.Ammo                   = "sniperpenetratedround"
SWEP.Primary.Automatic              = false
SWEP.Primary.HullSize               = 6
SWEP.DamageType                     = DMG_BULLET
SWEP.Primary.Spread                 = 0.01
SWEP.Primary.IronAccuracy           = 0.001
SWEP.Primary.Range 					= 40000
SWEP.Primary.RangeFalloff           = 0.8

SWEP.Primary.KickUp					= 0.1
SWEP.Primary.KickDown				= 0.1
SWEP.Primary.KickHorizontal			= 0.1
SWEP.Primary.StaticRecoilFactor 	= 0.2

SWEP.Primary.SpreadMultiplierMax 	= 6
SWEP.Primary.SpreadIncrement 		= 1
SWEP.Primary.SpreadRecovery 		= 5

SWEP.IronSightsPos 					= Vector(-3.145, -4, 2)
SWEP.IronSightsAng 					= Vector(0, 0, 0)

SWEP.RunSightsPos 					= Vector(0, 0, -0.202)
SWEP.RunSightsAng 					= Vector(-21.107, 25.326, -17.588)

SWEP.InspectPos 					= Vector(-5,-10,-2)
SWEP.InspectAng 					= Vector(20, 0, -20)

SWEP.Tracer                         = 0
SWEP.TracerCount 					= 0
SWEP.DrawCrosshair                  = true

SWEP.CustomMuzzleFlash 				= true
SWEP.MuzzleFlashEffect 				= "tfa_muzzle_sparks_energy"

SWEP.LuaShellEject 					= false
SWEP.BlowbackEnabled 				= true
SWEP.BlowbackVector 				= Vector(0.075,-3,0.00)
SWEP.Blowback_Shell_Effect 			= "ShellEject"

SWEP.Secondary.ScopeZoom			= 6
SWEP.RTMaterialOverride 			= 0
SWEP.Scoped 						= false
SWEP.BoltAction 					= false
SWEP.ScopeAngleTransforms = {
	{"R",0}, --Pitch
	{"Y",0}, --Yaw
	{"P",0}, --Roll
}
SWEP.ScopeOverlayTransforms 		= {0, 0}
SWEP.ScopeOverlayTransformMultiplier = 0.8
SWEP.IronSightsSensitivity 			= .3
SWEP.ScopeShadow 					= nil
SWEP.ScopeDirt 						= nil
SWEP.ScopeReticule_CrossCol 		= false
SWEP.ScopeReticule_Scale 			= {0, 0}

SWEP.Blowback_PistolMode 			= false
SWEP.BlowbackBoneMods 				= {
	--["Bolt"] = { scale = Vector(1, 1, 1), pos = Vector(-3.537, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.DoProceduralReload = true
SWEP.UseHands = false
SWEP.VMPos = Vector(0,0,0)
SWEP.VMAng = Vector(0,0,0)

SWEP.Offset = {
    Pos = {
        Up = 0,
        Right = 0,
        Forward = 0
    },
    Ang = {
        Up = 0,
        Right = 0,
        Forward = 0   
    }
}

function SWEP:Precache() 
 	util.PrecacheSound( self.ShootSound ) 
 end

 if SERVER then
function SWEP:Deploy()
    local ply = self:GetOwner()
    
    local AuthorizedUser = AuthorizedUser or {}

    local AuthorizedUser = {
            ["ONI"] = true,
            ["ONI Security Force"] = true,
    }

    local plyteam = ply:getJobTable().category

    if not AuthorizedUser[plyteam] then
        ply:StripWeapon("hard_sound_rifle")
    end
end
end

-- DEFINE_BASECLASS(SWEP.Base) -- If you have multiple overriden functions, place this line only over the first one

-- function SWEP:Think2(...) -- We're overriding Think2 without touching the main think function, which is called from there anyway
--     BaseClass.Think2(self, ...) -- THE MOST IMPORTANT LINE! It calls the Think2 function of the parent class, which is the base itself


-- 	if self.Owner:KeyPressed(IN_ATTACK2) and self:GetIronSights()==true then
-- 		self.Weapon:EmitSound("Weapon_rebirth_srs99.zoomin")
-- 	end
	
-- 	if self.Owner:KeyReleased(IN_ATTACK2) and self:GetIronSights()==false then
-- 		self.Weapon:EmitSound("Weapon_rebirth_srs99.zoomout")
-- 	end

--     local ViewModel = self.Owner:GetViewModel( )
 
--     local compassAng = (self.Owner:GetAimVector():Angle().y)
--     if compassAng > 22 and compassAng < 68      then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_nw" )
--     elseif compassAng > 67 and compassAng < 113     then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_w" )
--     elseif compassAng > 112 and compassAng < 157    then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_sw" )
--     elseif compassAng > 156 and compassAng < 202    then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_s" )
--     elseif compassAng > 201 and compassAng < 246    then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_se" )
--     elseif compassAng > 245 and compassAng < 291    then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_e" )
--     elseif compassAng > 290 and compassAng < 336    then
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_ne" )
--     else
--         ViewModel:SetSubMaterial( 8, "models/ishi/halo/weapons/srs99/compass_n" )
--     end
   
--     if CLIENT then
--         if self:IsCarriedByLocalPlayer() == false then
--             ViewModel:SetSubMaterial( 8, "" )
--         end
--     end
 
--     local ammoString = tostring( self:Clip1() )
--     local ammoOnes = string.Right( ammoString, 1 )
--     local ammoTens = string.Left( ammoString, 1 )
   
--     if self:Clip1() < 10 then
--         ammoTens = "0"
--     end
   
--     self.Bodygroups_V[8] =  tonumber( ammoTens )
--     self.Bodygroups_V[7] =  tonumber( ammoOnes )   
--     self.Bodygroups_W[8] =  tonumber( ammoTens )
--     self.Bodygroups_W[7] =  tonumber( ammoOnes )
	
-- end