if ( SERVER ) then
	AddCSLuaFile( "shared.lua" )
end

if CLIENT then
SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/hud/halo2_smg_dual" )
	
	killicon.Add( "tfa_halo2_smg_dual_anniversary", "VGUI/hud/halo2_smg_dual", color_white )
end
-- Variables that are used on both client and server
SWEP.Gun = ("tfa_halo2_smg_dual_anniversary") -- must be the name of your swep but NO CAPITALS!
SWEP.Category				= "Project Reclamation"
SWEP.Author					= "Stan & ΛTTLΛS"
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions			= "Defend Earth"
SWEP.PrintName				= "Dual M7/Caseless Submachine Guns"		-- Weapon name (Shown on HUD)	
SWEP.Slot					= 2				-- Slot in the weapon selection menu
SWEP.SlotPos				= 3			-- Position in the slot
SWEP.DrawAmmo				= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox		= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   	= false		-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= true		-- set false if you want no crosshair
SWEP.AutoDetectMuzzleAttachment = true
SWEP.Weight					= 30		-- rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		-- Auto switch from if you pick up a better weapon
SWEP.HoldType 				= "duel"		-- how others view you carrying the weapon
SWEP.XHair					= false		-- Used for returning crosshair after scope. Must be the same as DrawCrosshair
SWEP.BoltAction				= false	
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and crossbow make for good sniper rifles

SWEP.ViewModelFOV			= 75
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_smg_h2_aniversary_dual_fixed_muzz.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_irifle.mdl"	-- Weapon world model
SWEP.ShowWorldModel         = false
SWEP.Base 					= "tfa_gun_base"
SWEP.UseHands				= false

SWEP.ViewModelBoneMods = {
	["Bip01 R Forearm"] = { scale = Vector(1, 1, 1), pos = Vector(-5.37, 5.741, -0.186), angle = Angle(0, 0, 0) },
	["Bip01 L Forearm"] = { scale = Vector(1, 1, 1), pos = Vector(-5.37, 5.741, 3.888), angle = Angle(0, 0, 0) }
}


SWEP.Akimbo 				= true
SWEP.XHair					= true	-- Used for returning crosshair after scope. Must be the same as DrawCrosshair

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.Primary.Sound			= nil --("H2AN/smg/aniversary_smg_"..math.random(1,6)..".wav")		-- script that calls the primary fire sound
SWEP.Primary.SilencedSound 	= Sound("h2an/smg/suppressed_smg"..math.random(1,4)..".mp3")
SWEP.Primary.RPM				= 1400		-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 120		-- Size of a clip
SWEP.Primary.ClipSize_ExtRifle 	= 160
SWEP.Primary.DefaultClip		= 1312	-- Bullets you start with
SWEP.Primary.KickUp				= 0.2				-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.2			-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.0		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= true	-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "smg1"	-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.AmmoTypeStrings	= {["smg1"] = "M443 Caseless FMJ"}

SWEP.RunSightsPos = Vector(0.4, -20, -14.391)
SWEP.RunSightsAng = Vector(39.365, -0.872, 0)

SWEP.Primary.Damage		= 115	--base damage per bullet
SWEP.Primary.Spread		= 0.02	--define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = 0.025 -- ironsight accuracy, should be the same for shotguns

-- enter iron sight info and bone mod info below

SWEP.data 				= {}
SWEP.data.ironsights		= 1

SWEP.Secondary.IronFOV			= 75	-- How much you 'zoom' in. Less is more! 	

SWEP.data 				= {}
SWEP.data.ironsights		= 1

SWEP.IronSightsPos = Vector(2.006, 15.142, 0.101)
SWEP.IronSightsAng = Vector(0.153, 0, 0)

SWEP.VElements = {
	["muzzle_test"] = { type = "Sprite", sprite = "effects/ar2_altfire1", bone = "Bip01 L Forearm", rel = "", pos = Vector(38.96, 2.596, 4.675), size = { x = 0.01, y = 0.01 }, color = Color(0, 0, 255, 0), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["halo_silencer"] = { type = "Model", model = "models/attachments/tfa_att_sil.mdl", bone = "body.002", rel = "", pos = Vector(-9.674, 0.386, 0.744), angle = Angle(2.898, 93.952, 7.756), size = Vector(0.55, 0.55, 0.55), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, active = false, bodygroup = {} },
	["halo_silencer+"] = { type = "Model", model = "models/attachments/tfa_att_sil.mdl", bone = "body", rel = "", pos = Vector(-10.466, 0.57, 0.924), angle = Angle(2.898, 93.952, 7.756), size = Vector(0.55, 0.55, 0.55), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, active = false, bodygroup = {} },
	["laser_beam"] = { type = "Model", model = "models/tfa/lbeam.mdl", bone = "body.002", rel = "", pos = Vector(-6.769, 0.54, 2.012), angle = Angle(172.738, 4.664, 0), size = Vector(1, 1, 1), color = Color(255, 0, 0, 255), surpresslightning = false, active = false, material = "", skin = 0, bodygroup = {} },
	["laser_beam+"] = { type = "Model", model = "models/tfa/lbeam.mdl", bone = "body", rel = "", pos = Vector(-6.769, 0.54, 2.012), angle = Angle(172.738, 4.664, 0), size = Vector(1, 1, 1), color = Color(255, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {}, active = false },
}

SWEP.WElements = {
	["SMG_LEFT"] = { type = "Model", model = "models/models/pillar/type340smg.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(5.78, 1.2, 0), angle = Angle(-1.925, -91.471, 0.712), size = Vector(0.885, 0.885, 0.885), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["SMG_RIGHT"] = { type = "Model", model = "models/models/pillar/type340smg.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.558, 1.478, 0.185), angle = Angle(179.464, -104.664, 5.024), size = Vector(0.885, 0.885, 0.885), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["halo_silencer"] = { type = "Model", model = "models/attachments/tfa_att_sil.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "SMG_RIGHT", pos = Vector(0, -8.799, 4.636), angle = Angle(0, 0, 0), size = Vector(0.55, 0.55, 0.55), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, active = false, bodygroup = {} },
	["halo_silencer+"] = { type = "Model", model = "models/attachments/tfa_att_sil.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "SMG_LEFT", pos = Vector(0, -8.799, 4.636), angle = Angle(0, 0, 0), size = Vector(0.55, 0.55, 0.55), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, active = false, bodygroup = {} },
}
SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_silencer", "halo_sprintattack" }, order = 1 },
	[2] = { offset = { 0, 0 }, atts = { "halo_ub_laser", "halo_ext_mag_smg" }, order = 1 },
}

DEFINE_BASECLASS( SWEP.Base )
function SWEP:PrimaryAttack(...)
	if not self:CanPrimaryAttack() then return end
	if IsFirstTimePredicted() and not ( sp and CLIENT ) then
		if self:GetStat("Primary.SilencedSound") and self:GetSilenced() then
			self:EmitSound("h2an/smg/suppressed_smg3.mp3")
		else
			self:EmitSound("H2AN/smg/aniversary_smg_"..math.random(1,6)..".wav", 75, 100)
		end
	end
	return BaseClass.PrimaryAttack(self,...)
end
