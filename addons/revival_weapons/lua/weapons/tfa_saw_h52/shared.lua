if CLIENT then
    SWEP.WepSelectIcon             = surface.GetTextureID("vgui/hud/hr_swep_assault_rifle")
    
    killicon.Add( "tfa_hr_swep_assault_rifle", "vgui/hud/hr_swep_assault_rifle", color_white )
end

SWEP.Base = "tfa_gun_base"
SWEP.Category = "Project Reclamation"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.PrintName = "M739 SAW"
SWEP.Author				= "Chaosnarwhal | Kayn"
SWEP.Contact				= ""
SWEP.Purpose				= "shotty"
SWEP.Instructions				= "Just Shoot 'n' Kill :3"
SWEP.Slot				= 3				-- Slot in the weapon selection menu
SWEP.SlotPos				= 3			-- Position in the slot

SWEP.ViewModel			= "models/weapons/v_spartan_h4_saw.mdl" --Viewmodel path
SWEP.ViewModelFOV = 80
SWEP.VMPos = Vector(-0.2,0,0) --The viewmodel positional offset, constantly.  Subtract this from any other modifications to viewmodel position.
SWEP.VMAng = Vector(0,0,0) --The viewmodel angular offset, constantly.   Subtract this from any other modifications to viewmodel angl

SWEP.WorldModel			= "models/weapons/saw_w_h4.mdl" --Viewmodel path
SWEP.DefaultHoldType = "ar2"
SWEP.HoldType = "ar2"
SWEP.Offset = { --Procedural world model animation, defaulted for CS:S purposes.
        Pos = {
        Up = 1,
        Right = 1,
        Forward = 10,
        },
        Ang = {
        Up = 90,
        Right = 0,
        Forward = 190
        },
		Scale = 1
}

SWEP.Scoped = false
SWEP.Scoped_3D = false

SWEP.UseHands = false

SWEP.Shotgun = false
SWEP.ShellTime = 0.75

SWEP.DisableChambering 	= true
SWEP.Primary.ClipSize = 72
SWEP.Primary.DefaultClip = 588

SWEP.Primary.Sound = "weapons/halo 4 saw/saw 1.wav"
SWEP.Primary.Soundsilenced = "weapons/br85/gunfire/brsilenced.wav"
SWEP.Primary.Ammo = "ar2"
SWEP.Primary.Automatic 				= true
SWEP.Primary.RPM 					= 1200
SWEP.Primary.Damage 				= 130
SWEP.Primary.NumShots 				= 1
SWEP.Primary.HullSize = 1
SWEP.Primary.Knockback = 0
SWEP.Primary.Spread					= 0.3					--This is hip-fire acuracy.  Less is more (1 is horribly awful, .0001 is close to perfect)
SWEP.Primary.IronAccuracy 			= .005	-- Ironsight accuracy, should be the same for shotguns
SWEP.SelectiveFire 					= false
SWEP.OnlyBurstFire					= false --No auto, only burst?

SWEP.Primary.KickUp					= 0.3					-- This is the maximum upwards recoil (rise)
SWEP.Primary.KickDown				= 0.3					-- This is the maximum downwards recoil (skeet)
SWEP.Primary.KickHorizontal			= 0.01				-- This is the maximum sideways recoil (no real term)
SWEP.Primary.StaticRecoilFactor 	= 0.05 	--Amount of recoil to directly apply to EyeAngles.  Enter what fraction or percentage (in decimal form) you want.  This is also affected by a convar that defaults to 0.5.

SWEP.Primary.SpreadMultiplierMax = 1.5 --How far the spread can expand when you shoot.
SWEP.Primary.SpreadIncrement = 1 --What percentage of the modifier is added on, per shot.
SWEP.Primary.SpreadRecovery = 1.5 --How much the spread recovers, per second.

SWEP.Secondary.IronFOV = 60 --Ironsights FOV (90 = same)
SWEP.BoltAction = false --Un-sight after shooting?
SWEP.BoltTimerOffset = 0.25 --How long do we remain in ironsights after shooting?

SWEP.IronSightsPos = Vector(-5.8, 0, -1.8)
SWEP.IronSightsAng = Vector(0, 0, -0.5)

SWEP.RunSightsPos = Vector(0, 0, 0)
SWEP.RunSightsAng = Vector(-11.869, 17.129, -16.056)

SWEP.InspectionPos = Vector(12.8, -10.653, -4.19)
SWEP.InspectionAng = Vector(36.389, 48.549, 22.513)
SWEP.ReticleScale = 0.7

SWEP.DrawCrosshairIS = true
SWEP.Primary.Range = 16*164.042*10 -- The distance the bullet can travel in source units.  Set to -1 to autodetect based on damage/rpm.
SWEP.Primary.RangeFalloff = 0.8 -- The percentage of the range the bullet damage starts to fall off at.  Set to 0.8, for example, to start falling off after 80% of the range.

SWEP.BlowbackEnabled = false
SWEP.BlowbackVector = Vector(0,-2.0,0)
SWEP.Blowback_Shell_Effect = ""

SWEP.ScopeScale = 0.5 --Scale of the scope overlay
SWEP.ReticleScale = 0.7 --Scale of the reticle overlay

SWEP.MuzzleFlashEffect = "h3_br_flash"
SWEP.MuzzleFlashEnabled = false --Enable muzzle flash
SWEP.AutoDetectMuzzleAttachment = false --For multi-barrel weapons, detect the proper attachment?

SWEP.data = {}
SWEP.data.ironsights = 1 --Enable Ironsigh

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)

local IronSightSound1 = Sound( "weapons/generic/ironsight_on.wav" );
local IronSightSound2 = Sound( "weapons/generic/ironsight_off.wav" );

SWEP.Attachments = {
	[1] = { offset = { 0, 0 }, atts = { "halo_fmj_saw", "halo_ext_mag", "halo_rapidfire" }, order = 1 },
	[2] = { offset = { 0, 0 }, atts = { "halo_sprintattack"}, order = 2},
}
